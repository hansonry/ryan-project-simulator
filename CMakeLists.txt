cmake_minimum_required(VERSION 3.10)

# set the project name
project(ryan-project-simulator)

# Uncomment following line to enable debug builds
#set(CMAKE_BUILD_TYPE Debug)



# add the executable
add_library(ryan-project-simulator 
   STATIC 
      src/ryanprojectsimulator.c
      src/Hour.c)
target_include_directories(ryan-project-simulator PUBLIC include src)

# Max warnings
if(MSVC)
  target_compile_options(ryan-project-simulator PRIVATE /W4 /WX)
else()
  target_compile_options(ryan-project-simulator PRIVATE -Wall -Wextra -pedantic -Werror)
endif()

# examples
add_subdirectory(example)
add_subdirectory(test)
