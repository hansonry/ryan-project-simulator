#include <ryanmock.h>
#include <ryanprojectsimulator.h>

void test_OneDay(void)
{
   struct tm time = {
      .tm_sec = 0,
      .tm_min = 0,
      .tm_hour = 0,
      .tm_mday = 1, // Jan 1
      .tm_mon = 0,
      .tm_year = 122,
   };
   struct tm time2 = {
      .tm_sec = 0,
      .tm_min = 0,
      .tm_hour = 0,
      .tm_mday = 2, // Jan 2
      .tm_mon = 0,
      .tm_year = 122,
   };

   struct hour startHour, endHour;
   Hour_SetFromTmStruct(&startHour, &time);
   Hour_SetFromTmStruct(&endHour,   &time2);
   struct rps_task task = {
      .hoursToComplete = 8,
      
   };
   struct rps_resource ryan = {
      .name = "Ryan",
      .hoursPerDay = {0, 8, 8, 8, 8, 8, 0},
   };
   struct rps_config config = {
      .resources = &ryan,
      .resourceCount = 1,
      .tasks = &task,
      .taskCount = 1,
      .startHour = startHour,
   };
   
   // FUT
   rps_simulate(&config);
   
   rmmAssertUIntEqual(task.startHour.time, startHour.time);
   rmmAssertUIntEqual(task.endHour.time, endHour.time);

}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_OneDay),

   };
   return rmmRunTestsCmdLine(tests, "ryanprojectsimulator", argc, args);
}


