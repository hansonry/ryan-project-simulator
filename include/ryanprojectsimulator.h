
#include <Hour.h>

struct rps_task
{
   // Input
   int hoursToComplete;
   
   
   // Working
   int hoursSoFar;
   
   // Output
   struct hour startHour;
   struct hour endHour;
};

struct rps_resource
{
   // Input
   const char * name;
   int hoursPerDay[eDOW_Last];
   
   // Working
   int hoursRemaingToday;

   // Output
   int hoursWorked;
};

struct rps_config
{
   // Input
   struct rps_resource * resources;
   size_t resourceCount;
   struct rps_task * tasks;
   size_t taskCount;
   struct hour startHour;
   
   // Output
   struct hour endHour;
};



void rps_simulate(struct rps_config * config);




