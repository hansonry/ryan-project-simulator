#ifndef __HOUR_H__
#define __HOUR_H__
#include <time.h>

struct hour
{
   time_t time;
};

enum dayOfWeek
{
   eDOW_Sunday,
   eDOW_Monday,
   eDOW_Tuesday,
   eDOW_Wednesday,
   eDOW_Thursday,
   eDOW_Friday,
   eDOW_Saturday,
   eDOW_Last,
};

struct hour * Hour_SetToday(struct hour * hour);
struct hour * Hour_SetFromTime(struct hour * hour, time_t time);
struct hour * Hour_SetFromTmStruct(struct hour * hour, const struct tm * tm);
struct hour * Hour_Copy(struct hour * dest, const struct hour * src);
time_t Hour_GetTime(const struct hour * hour);
struct hour * Hour_AddDays(struct hour * hour, int days);
struct hour * Hour_AddHours(struct hour * hour, int hours);
struct hour * Hour_MoveToStartOfDay(struct hour * hour);
enum dayOfWeek Hour_GetDayOfWeek(const struct hour * hour);


#endif // __HOUR_H__
