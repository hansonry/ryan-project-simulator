#include <stdbool.h>
#include <ryanprojectsimulator.h>

static inline
bool rps_isCurrentTaskFinished(const struct rps_task * task)
{
   return task->hoursSoFar >= task->hoursToComplete;
   
}

static inline
bool rps_areAllTasksCompleted(struct rps_config * config)
{
   size_t i;
   for(i = 0; i < config->taskCount; i++)
   {
      struct rps_task * task = &config->tasks[i];
      if(!rps_isCurrentTaskFinished(task))
      {
         return false;
      }
   }
   return true;
}

static inline
void rps_initialize(struct rps_config * config)
{
   size_t i;
   for(i = 0; i < config->taskCount; i++)
   {
      config->tasks[i].hoursSoFar = 0;
   }
}

static inline
void rps_setResourceHoursRemainingForToday(struct rps_config * config, const struct hour * hourTime)
{
   size_t i;
   for(i = 0; i < config->resourceCount; i++)
   {
      struct rps_resource * resource = &config->resources[i];
      resource->hoursRemaingToday = resource->hoursPerDay[Hour_GetDayOfWeek(hourTime)];
   }
   
}


void rps_simulate(struct rps_config * config)
{
   struct rps_task * currentTask = &config->tasks[0];
   struct hour hourTime;
   int hourCount = 0;
   Hour_Copy(&hourTime, &config->startHour);
   rps_initialize(config);
   rps_setResourceHoursRemainingForToday(config, &hourTime);
   
   Hour_Copy(&currentTask->startHour, &hourTime);
   while(!rps_areAllTasksCompleted(config))
   {
      currentTask->hoursSoFar ++;
      hourCount ++;
      
      if(hourCount >= 8)
      {
         hourCount -= 8;
         Hour_AddDays(&hourTime, 1);
         rps_setResourceHoursRemainingForToday(config, &hourTime);
      }
      if(rps_isCurrentTaskFinished(currentTask))
      {
         Hour_Copy(&currentTask->endHour, &hourTime);
      }
   }
}
