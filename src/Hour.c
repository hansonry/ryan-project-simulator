#include "Hour.h"
#include <string.h>

#define ONE_HOUR (60 * 60)
#define ONE_DAY (ONE_HOUR * 24)


#define STDC_VERSION_C11 201112  
#define STDC_VERSION_C23 202312  
 


#if __STDC_VERSION__ < STDC_VERSION_C11
#error C versions less than C11 are not supported
#elif __STDC_VERSION__ < STDC_VERSION_C23
#define LOCALTIME(tm, time) localtime_s(tm, &time)
#else
#define LOCALTIME(tm, time) localtime_r(time, tm)
#endif


static inline
void Hour_SetAtBeginningOfHour(struct hour * hour)
{
   struct tm timeParts;
   LOCALTIME(&timeParts, hour->time);
   timeParts.tm_min = 0;
   timeParts.tm_sec = 0;
   hour->time = mktime(&timeParts);
}

struct hour * Hour_SetToday(struct hour * hour)
{
   hour->time = time(NULL);
   Hour_SetAtBeginningOfHour(hour);
   return hour;
}

struct hour * Hour_SetFromTime(struct hour * hour, time_t time)
{
   hour->time = time;
   Hour_SetAtBeginningOfHour(hour);
   return hour;   
}

struct hour * Hour_SetFromTmStruct(struct hour * hour, const struct tm * tm)
{
   struct tm temp;
   memcpy(&temp, tm, sizeof(struct tm));
   hour->time = mktime(&temp);
   Hour_SetAtBeginningOfHour(hour);
   return hour;
}

struct hour * Hour_Copy(struct hour * dest, const struct hour * src)
{
   dest->time = src->time;
   return dest;
}

time_t Hour_GetTime(const struct hour * hour)
{
   return hour->time;
}

struct hour * Hour_AddDays(struct hour * hour, int days)
{
   hour->time += (time_t)days * ONE_DAY;
   return hour;
}

struct hour * Hour_AddHours(struct hour * hour, int hours)
{
   hour->time += (time_t)hours * ONE_HOUR;
   return hour;
}

struct hour * Hour_MoveToStartOfDay(struct hour * hour)
{
   struct tm timeParts;
   LOCALTIME(&timeParts, hour->time);
   timeParts.tm_hour = 0;
   timeParts.tm_min  = 0;
   timeParts.tm_sec  = 0;
   hour->time = mktime(&timeParts);
   return hour;
}

enum dayOfWeek Hour_GetDayOfWeek(const struct hour * hour)
{
   struct tm timeParts;
   (void)LOCALTIME(&timeParts, hour->time);
   return (enum dayOfWeek)timeParts.tm_wday;
}


